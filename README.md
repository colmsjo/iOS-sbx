CocoaPods
---------

[CocoaPods](https://cocoapods.org) dependency mangement for iOS.

```
gem update --system
gem install cocoapods
pod setup
```

`Search for and download a pod:

```
pod search treeview


```


Resources:

 * http://www.raywenderlich.com/64546/introduction-to-cocoapods-2
